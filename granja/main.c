#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_NOM 35
#define MAX_ESPECIE 50
#define BB while(getchar()!='\n')
# define MAX_COS 10000


struct granja{
    char nom[MAX_NOM+1];
    int codi;
    char especie[MAX_ESPECIE+1];
};

void entradaAnimals(struct granja *p);
void imprimirAnimals(struct granja p);
int ecriuregranja(FILE *fitxerGranja,struct granja *p, int nReg);
int llegirgranja(FILE *fitxerGranja,struct granja *p, int nReg);
void llistahtml();

int main()
{
    struct granja p ;
    FILE *fitxerGranja;
    int nAnimals;
    int n,error=0;
    FILE *f;
    char cos[MAX_COS]="";
    char cadenaTMP[100];
    char cap[]="<!DOCTYPE html> <html lang=\"en\"> \
        <head> \
            <meta charset=\"UTF-8\">\
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\
            <title>Document</title>\
        </head>\
        <body> \
          <table border=1><tr><th>nom</th><th>codi</th><th>especie</th></tr>";

    char peu[]="\n</table></body>\n</html>";


    printf("Quants animals vols donar d'alta? ");
    scanf("%d",&nAnimals);BB;

    //obrir fitxerGranja escriptura
    fitxerGranja= fopen("fitxerGranja.bin","ab");//fitxerGranja binari (b) d'afegir (a)
    if(fitxerGranja==NULL){
        error=1; //error obrir fitxerGranja
    }else{
        for(int i=0;i<nAnimals;i++){
            entradaAnimals(&p);
            ecriuregranja(fitxerGranja, &p, 1);
        }
    }
    if(fclose(fitxerGranja)){
        printf("error al tancar el fitxerGranja...");
    }

    printf("\n***LECTURA DE ANIMALS DEL fitxerGranja****\n");
    //obrir llegir escriptura
    fitxerGranja= fopen("fitxerGranja.bin","rb");//fitxerGranja binari (b) d'afegir (a)
    if(fitxerGranja==NULL){
        error=1; //error obrir fitxerGranja
    }
    while(!feof(fitxerGranja)){
        n=llegirgranja(fitxerGranja, &p, 1);
        if(n==0){
            inprimirAnimals(p);
        }
        if(n==1){
            printf("\nError en obrir fitxerGranja...\n");break;
        }
        if(n==3){
            printf("\nError de lectura...\n");break;
        }
    }
    if(fclose(fitxerGranja)){
        printf("error al tancar el fitxerGranja...");
    }

    //llistat html
    llistahtml();


    return error;
}

void llistahtml(){
    struct granja p ;
    FILE *fitxerGranja;
    int nAnimals;
    int n,error=0;
    FILE *f;
    char cos[MAX_COS]="";
    char cadenaTMP[100];
    char cap[]="<!DOCTYPE html> <html lang=\"en\"> \
        <head> \
            <meta charset=\"UTF-8\">\
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\
            <title>Document</title>\
        </head>\
        <body> \
          <table border=1><tr><th>nom</th><th>codi</th><th>especie</th></tr>";

    char peu[]="\n</table></body>\n</html>";

    f=fopen("index.html", "w");
        if(f==NULL){
            error=1;
        }else{
        //no hi ha error!!

        error=fputs(cap, f);
        if(error==EOF){
            error=2;
        }
         fitxerGranja= fopen("fitxerGranja.bin","rb");//fitxerGranja binari (b) d'afegir (a)
        if(fitxerGranja==NULL){
            error=1; //error obrir fitxerGranja
        }

        while(!feof(fitxerGranja)){
            //GENERAR LES FILES DE LA TAULA
            //llegiir persona del fitxer binari
             n=llegirgranja(fitxerGranja, &p, 1);
            if(n==0){
                //inprimirAnimals(p);
            }
            if(n==1){
                printf("\nError en obrir fitxerGranja...\n");break;
            }
            if(n==3){
                printf("\nError de lectura...\n");break;
            }

            strcat(cos, "<tr><td>");
            strcat(cos, p.nom);
            strcat(cos,"</td><td>");
            sprintf(cadenaTMP, "%d", p.codi);//connvertir numero en cadena
            strcat(cos,cadenaTMP);
            strcat(cos,"</td><td>");
            strcat(cos,p.especie);
            strcat(cos,"</td></tr>");
        }

        error=fputs(cos, f);
        if(error==EOF){
            error=2;
        }

        error=fputs(peu,f);
        if(fclose(f)){
            error=3;
        }

        if(fclose(fitxerGranja)){
            printf("error al tancar el fitxerGranja...");
        }

    }
}

void entradaAnimals(struct granja *p){
    printf("Nom: ");
    scanf("%35[^\n]",p->nom);BB;// (*p).nom
    printf("code: ");
    scanf("%i",&p->codi);BB;// &(*p).codi
    printf("especie: ");
    scanf("%50[^\n]",p->especie);BB;// (*p).especie
}

void inprimirAnimals(struct granja p){
    printf("Nom: %s\n",p.nom);
    printf("codi: %i\n", p.codi);
    printf("especie: %s\n", p.especie);
}

int ecriuregranja(FILE *fitxerGranja,struct granja *p, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxerGranja persones.bin
    n=fwrite(p,sizeof(struct granja),nReg,fitxerGranja);
    if(n!=nReg){
        error=2; //error numero registres escrits
    }
    return error;
}

/*
Retorn de tipus enter. Interpretació:
    0 -> no error. Hi ha les dades llegides del fitxerGranja al paràmetre *p
    1 -> error obrir fitxerGranja
    3 -> error de lectura
    4 -> Advertència. Ha arribat al final del fitxerGranja.
*/
int llegirgranja(FILE *fitxerGranja,struct granja *p, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxerGranja persones.bin
    n=fread(p,sizeof(struct granja),nReg,fitxerGranja);
    if(!feof(fitxerGranja)){
        if(n==0){
            error=3; //error de lectura
        }
    }else{
        error=4 ; //És un avís. Ha arribat al final del fitxerGranja.
    }
    return error;
}

